import React, { useState } from 'react';
import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";

const DUMMY_EXPENSES = [
  {
    id: 'e1',
    title: 'Food',
    amount: 6414.67,
    date: new Date(2021,2,12)
  },
  {
    id: 'e2',
    title: 'Rent the appartment',
    amount: 2324.67,
    date: new Date(2020,2,12)
  },
  {
    id: 'e3',
    title: 'Yoga',
    amount: 98.7,
    date: new Date(2021,2,12)
  },
  {
    id: 'e4',
    title: 'Flowers',
    amount: 4.5,
    date: new Date(2019,2,12)
  }
]

function App() {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const addExpenseHandler = (expense) => {
    // console.log(expense)
    setExpenses(prevExpenses => {
        return [expense, ...prevExpenses];
    });

  };

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler}/>
      <Expenses expenses={expenses} />
    </div>
  )
}

export default App;