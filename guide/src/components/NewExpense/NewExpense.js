import React, { useState } from "react";
import ExpreseForm from "../ExpensesForm/ExpensesForm";
import "./NewExpense.css";

const NewExpense = (props) => {
    const [formValue, setFormValue] = useState(false);

    const saveExpenseDataHandler = (enteredExpenseData) => {

        const expenseData = {
            ...enteredExpenseData,
            id: Math.random().toString()
        };
        props.onAddExpense(expenseData);
        setFormValue(false);

    }
    const handleFormView = () => {
        setFormValue(true);

    }

    console.log(formValue);

    return (
        <div className="new-expence">
            {formValue ? <ExpreseForm onSaveExpenseData={saveExpenseDataHandler} setFormValue={setFormValue} /> : <button onClick={handleFormView} value={formValue}>Add expense</button> }
        </div>
    )
};

export default NewExpense;