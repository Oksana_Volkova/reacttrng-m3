import ExpensesFilter from '../ExpensesFilter/ExpensesFilter';
import React, { useState } from 'react';
import './Expenses.css';
import ExpensesList from '../ExpensesList/ExpensesList';
import ExpemsesChart from './ExpensesChart';


const Expenses = (props) => {
    const [filteredYear, setFilteredYear] = useState("2020");

    const filterChangeHandler = selectedYear => {
        setFilteredYear(selectedYear);
    };

    const filteredExpenses = props.expenses.filter(expense => {
        return expense.date.getFullYear().toString() === filteredYear;
    });

    let expensesContent = <p>No expenses found.</p>

    return (
        <div className="expenses">
           <ExpensesFilter
                selected={filteredYear}
                onChangeFilter={filterChangeHandler}
            />
            <ExpemsesChart expenses={filteredExpenses}/>
            <ExpensesList items={filteredExpenses}/>
            {expensesContent}
        </div>
    );
};

export default Expenses;